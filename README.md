# Paradigr

## Installation
Paradigr can be build like any other Go programm using `go build`:
```
go build paradigr.go
```

## Usage
The following flags need to be specified:
* `-i` – a path to the input [Morfflex](https://ufal.mff.cuni.cz/morfflex) file
* `-o` – a path to the output folder
* `-l` – an ISO 639 language code of the language of input file (only Czech `ces` and Slovak `slk` are supported)

```
./paradigr -i=/path/to/morfflex_file.tsv -o=/path/to/output_folder -l=ces
```

## License
Paradigr is licesed under the MIT license. See LICENSE.md for details.
