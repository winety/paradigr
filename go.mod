module paradigr

go 1.22

toolchain go1.22.2

require (
	github.com/go-json-experiment/json v0.0.0-20240418180308-af2d5061e6c2
	golang.org/x/text v0.14.0
)
