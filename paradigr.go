// Author: Tomáš Vlček <tvlcek@mail.muni.cz>
// License: MIT License
package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/go-json-experiment/json"

	"golang.org/x/text/unicode/norm"
)

// A triplet is a list (slice) of three strings:
// 1. a lemma of a verb form,
// 2. a morphological tag,
// 3. a verb form.
type Triplet []string

type Finite struct {
	Singular1st []string `json:"1/sg,omitempty"`
	Singular2nd []string `json:"2/sg,omitempty"`
	Singular3rd []string `json:"3/sg,omitempty"`
	Plural1st   []string `json:"1/pl,omitempty"`
	Plural2nd   []string `json:"2/pl,omitempty"`
	Plural3rd   []string `json:"3/pl,omitempty"`
}

type Imperative struct {
	Singular2nd []string `json:"2/sg,omitempty"`
	Plural1st   []string `json:"1/pl,omitempty"`
	Plural2nd   []string `json:"2/pl,omitempty"`
}

type Gerund struct {
	SingularMasculine []string `json:"masc/sg,omitempty"`
	SingularNeutral   []string `json:"neut/sg,omitempty"`
	SingularFeminine  []string `json:"fem/sg,omitempty"`
	Plural            []string `json:"pl/,omitempty"`
}

type Meta struct {
	Category string `json:"category,omitempty"`
	Asp      Aspect `json:"asp,omitempty"`
}

type Aspect struct {
	Aspect string `json:"aspect,omitempty"`
}

type Paradigm struct {
	Meta       Meta       `json:"meta,omitempty"`
	Infinitive []string   `json:"infinitive,omitempty"`
	Passive    []string   `json:"passive,omitempty"`
	Participle []string   `json:"participle,omitempty"`
	Finite     Finite     `json:"finite,omitzero"`
	Imperative Imperative `json:"imperative,omitzero"`
	Gerund     Gerund     `json:"gerund,omitzero"`
	Perfective Gerund     `json:"gerund.perfective,omitzero"`
}

type Tagset struct {
	infinitive  string
	Passive     string
	participle  string
	finite_sg_1 string
	finite_sg_2 string
	finite_sg_3 string

	finite_pl_1 string
	finite_pl_2 string
	finite_pl_3 string

	imperative_sg_2 string
	imperative_pl_1 string
	imperative_pl_2 string

	gerund_sg_fem  string
	gerund_sg_masc string
	gerund_sg_neut string
	gerund_pl      string

	perfective_sg_fem  string
	perfective_sg_masc string
	perfective_sg_neut string
	perfective_pl      string
}

func normalizeString(input string) string {
	return norm.NFC.String(input)
}

func normalizeStrings(input_list []string) []string {
	var output_list []string
	for _, input_string := range input_list {
		output_list = append(output_list, normalizeString(input_string))
	}
	return output_list
}

// »findMatchingVerbForms« returns a list of verbs from the list »triplets« whose morphological
// tags match the tag specified in »tag«. If no verb forms match, an empty list is returned.
func findMatchingVerbForms(triplets []Triplet, tag string) []string {
	var verbs []string
	r, _ := regexp.Compile(tag)
	for _, triplet := range triplets {
		current_verb_tag := triplet[1]
		if r.MatchString(current_verb_tag) {
			verbs = append(verbs, triplet[2])
		}
	}
	return verbs
}

func getAspectFromTag(tag string) string {
	thirteenth_position := string([]rune(tag)[12])

	switch thirteenth_position {
	case "P":
		return "perfective"
	case "I":
		return "imperfective"
	case "B":
		return "biaspectual"
	default:
		return ""
	}
}

// »fillVerbalParadigm« transforms a list of triplets into a verbal paradigm. Each cell is
// filled with a list of all verb forms from »triplets« that match the morphological tag from
// the tagset specified in »tags«. If no verb forms match the cell, the cell will contain
// an empty list.
func fillVerbalParadigm(triplets []Triplet, tags Tagset) Paradigm {
	par := Paradigm{}

	par.Meta.Category = "v"
	par.Meta.Asp.Aspect = getAspectFromTag(triplets[0][1])

	par.Infinitive = findMatchingVerbForms(triplets, tags.infinitive)
	if len(par.Infinitive) == 0 {
		par.Infinitive = append(par.Infinitive, triplets[0][0])
	}
	par.Passive = findMatchingVerbForms(triplets, tags.Passive)
	par.Participle = findMatchingVerbForms(triplets, tags.participle)

	par.Finite.Singular1st = findMatchingVerbForms(triplets, tags.finite_sg_1)
	par.Finite.Singular2nd = findMatchingVerbForms(triplets, tags.finite_sg_2)
	par.Finite.Singular3rd = findMatchingVerbForms(triplets, tags.finite_sg_3)

	par.Finite.Plural1st = findMatchingVerbForms(triplets, tags.finite_pl_1)
	par.Finite.Plural2nd = findMatchingVerbForms(triplets, tags.finite_pl_2)
	par.Finite.Plural3rd = findMatchingVerbForms(triplets, tags.finite_pl_3)

	par.Imperative.Singular2nd = findMatchingVerbForms(triplets, tags.imperative_sg_2)
	par.Imperative.Plural1st = findMatchingVerbForms(triplets, tags.imperative_pl_1)
	par.Imperative.Plural2nd = findMatchingVerbForms(triplets, tags.imperative_pl_2)

	par.Gerund.SingularFeminine = findMatchingVerbForms(triplets, tags.gerund_sg_fem)
	par.Gerund.SingularMasculine = findMatchingVerbForms(triplets, tags.gerund_sg_masc)
	par.Gerund.SingularNeutral = findMatchingVerbForms(triplets, tags.gerund_sg_neut)
	par.Gerund.Plural = findMatchingVerbForms(triplets, tags.gerund_pl)

	par.Perfective.SingularFeminine = findMatchingVerbForms(triplets, tags.perfective_sg_fem)
	par.Perfective.SingularMasculine = findMatchingVerbForms(triplets, tags.perfective_sg_masc)
	par.Perfective.SingularNeutral = findMatchingVerbForms(triplets, tags.perfective_sg_neut)
	par.Perfective.Plural = findMatchingVerbForms(triplets, tags.perfective_pl)

	return par
}

// »printOutVerbalParadigm« prints out a part of a verbal paradigm »par« to the standdard output.
func printOutVerbalParadigm(par Paradigm) {
	fmt.Println("INFINITIV atp.")
	fmt.Printf("%v\tinf\t%v\n", par.Infinitive[0], par.Infinitive)
	fmt.Printf("%v\tpas\t%v\n", par.Infinitive[0], par.Passive)
	fmt.Printf("%v\tpart\t%v\n", par.Infinitive[0], par.Participle)
	fmt.Println("FINITUM")
	fmt.Printf("%v\tfs1\t%v\n", par.Infinitive[0], par.Finite.Singular1st)
	fmt.Printf("%v\tfs2\t%v\n", par.Infinitive[0], par.Finite.Singular2nd)
	fmt.Printf("%v\tfs3\t%v\n", par.Infinitive[0], par.Finite.Singular3rd)
	fmt.Printf("%v\tfp1\t%v\n", par.Infinitive[0], par.Finite.Plural1st)
	fmt.Printf("%v\tfp2\t%v\n", par.Infinitive[0], par.Finite.Plural2nd)
	fmt.Printf("%v\tfp3\t%v\n", par.Infinitive[0], par.Finite.Plural3rd)
	fmt.Println("IMPERATIV")
	fmt.Printf("%v\tis2\t%v\n", par.Infinitive[0], par.Imperative.Singular2nd)
	fmt.Printf("%v\tip1\t%v\n", par.Infinitive[0], par.Imperative.Plural1st)
	fmt.Printf("%v\tip2\t%v\n", par.Infinitive[0], par.Imperative.Plural2nd)
	fmt.Println("GERUND")
	fmt.Printf("%v\tgsf\t%v\n", par.Infinitive[0], par.Gerund.SingularFeminine)
	fmt.Printf("%v\tgsm\t%v\n", par.Infinitive[0], par.Gerund.SingularMasculine)
	fmt.Printf("%v\tgsn\t%v\n", par.Infinitive[0], par.Gerund.SingularNeutral)
	fmt.Printf("%v\tgp\t%v\n", par.Infinitive[0], par.Gerund.Plural)
	fmt.Println("PERFECTIVE")
	fmt.Printf("%v\tpsf\t%v\n", par.Infinitive[0], par.Perfective.SingularFeminine)
	fmt.Printf("%v\tpsm\t%v\n", par.Infinitive[0], par.Perfective.SingularMasculine)
	fmt.Printf("%v\tpsn\t%v\n", par.Infinitive[0], par.Perfective.SingularNeutral)
	fmt.Printf("%v\tpp\t%v\n", par.Infinitive[0], par.Perfective.Plural)
	fmt.Println()
}

// »createFileWithFolder« creates a file specified in »path«. It creates all the subfolders in
// the filepath, if they do not exist yet.
func createFileWithFolder(path string) (*os.File, error) {
	err := os.MkdirAll(filepath.Dir(path), 0770)
	if err != nil {
		return nil, err
	}
	return os.Create(path)
}

// »saveParadigm« saves the paradigm »par« into a file »output_filepath« in the JSON format.
func saveParadigm(par Paradigm, output_filepath string) error {
	output_file, file_creation_error := createFileWithFolder(output_filepath)
	if file_creation_error != nil {
		os.Stderr.WriteString("E7: Couldn't create file " + output_filepath + " .")
		return file_creation_error
	}
	defer output_file.Close()

	json_output, marshaling_error := json.Marshal(par)
	if marshaling_error != nil {
		os.Stderr.WriteString("E8: Couldn't convert struct to JSON.")
		return marshaling_error
	}

	output_file.Write(json_output)
	return nil
}

// »processTriplets« processes a list of triplets into a verbal paradigm based on the tagset
// specified in »tags«. It then saves this paradigm as a JSON file into a subfolder (whose
// name is the first letter of the processed verb) of the folder specified in »output_folder«.
func processTriplets(triplets []Triplet, tags Tagset, output_folder string) error {
	paradigm := fillVerbalParadigm(triplets, tags)

	filename := paradigm.Infinitive[0]
	last_folder := string([]rune(filename)[0:1])
	output_file := filepath.Join(output_folder, last_folder, filename)

	err := saveParadigm(paradigm, output_file)
	if err != nil {
		return err
	}
	return nil
}

// »areTripletsVerbs« reports whether a list of »triplets« is a set of verbs.
func areTripletsVerbs(triplets []Triplet) bool {
	if len(triplets) > 0 && len(triplets[0]) == 3 && len(triplets[0][1]) > 0 {
		first_verb_tag := triplets[0][1]
		return strings.HasPrefix(first_verb_tag, "V")
	} else {
		return false
	}
}

// »createTagset« creates a tagset based on the ISO 639-3 language code specified
// in »language_code«.
func createTagset(language_code string) Tagset {
	tags := Tagset{}

	if language_code == "ces" { // Czech tagset
		tags.infinitive = "Vf--------A-.-."
		tags.Passive = "VsYS---.X-A..-."
		tags.participle = "VpYS---.R-A..--"

		tags.finite_sg_1 = "VB-S---1P-A..-."
		tags.finite_sg_2 = "VB-S---2P-A..-."
		tags.finite_sg_3 = "VB-S---3P-A..-."

		tags.finite_pl_1 = "VB-P---1P-A...."
		tags.finite_pl_2 = "VB-P---2P-A..-."
		tags.finite_pl_3 = "VB-P---3P-A..-."

		tags.imperative_sg_2 = "Vi-S---2--A-.-."
		tags.imperative_pl_1 = "Vi-P---1--A-.-."
		tags.imperative_pl_2 = "Vi-P---2--A-.-."

		tags.gerund_sg_fem = "VeHS------A-.-."
		tags.gerund_sg_masc = "VeYS------A-.-."
		tags.gerund_sg_neut = "VeHS------A-.-."
		tags.gerund_pl = "VeXP------A-.-."

		tags.perfective_sg_fem = "VmHS------A-.-."
		tags.perfective_sg_masc = "VmYS------A-.-."
		tags.perfective_sg_neut = "VmHS------A-.-."
		tags.perfective_pl = "VmXP------A-.-."

	} else if language_code == "slk" { // Slovak tagset
		tags.infinitive = "Vf--------A-.-."
		tags.Passive = "Vs[MY]S---.X-A..-."
		tags.participle = "VpYS---.R-A..--"

		tags.finite_sg_1 = "VB-S---1P-A..-."
		tags.finite_sg_2 = "VB-S---2P-A..-."
		tags.finite_sg_3 = "VB-S---3P-A..-."

		tags.finite_pl_1 = "VB-P---1P-A...."
		tags.finite_pl_2 = "VB-P---2P-A..-."
		tags.finite_pl_3 = "VB-P---3P-A..-."

		tags.imperative_sg_2 = "Vi-S---2--A-.-."
		tags.imperative_pl_1 = "Vi-P---1--A-.-."
		tags.imperative_pl_2 = "Vi-P---2--A-.-."

		tags.gerund_sg_fem = "VeXX------A----"
		tags.gerund_sg_masc = "VeXX------A----"
		tags.gerund_sg_neut = "VeXX------A----"
		tags.gerund_pl = "VeXX------A----"

		tags.perfective_sg_fem = "VmHS------A-.-."
		tags.perfective_sg_masc = "VmYS------A-.-."
		tags.perfective_sg_neut = "VmHS------A-.-."
		tags.perfective_pl = "VmXP------A-.-."
	}
	return tags
}

func main() {
	// User input handling --------------------------------------------------------------------
	var user_specified_language string
	var input_filepath string
	var output_filepath string
	var tagset Tagset

	flag.StringVar(&input_filepath, "i", "", "a path to the input file")
	flag.StringVar(&output_filepath, "o", "", "a path to the folder where the output files shall be saved to")
	flag.StringVar(&user_specified_language, "l", "", "an ISO 639 language code of a language (only Czech and Slovak are supported)")
	flag.Parse()

	// Checks whenever filepaths were specified.
	if input_filepath == "" || output_filepath == "" {
		os.Stderr.WriteString("E1: Input or output filepath wasn't specified.\n")
		os.Exit(1)
	}

	// Check whenever the input file exists. If it doesn't, quit.
	if _, err := os.Stat(input_filepath); err != nil {
		os.Stderr.WriteString("E2: Input file " + input_filepath + " doesn't exist.\n")
		os.Exit(2)
	}

	// Check whenever the output folder exist. If it doesn't, quit.
	if _, err := os.Stat(output_filepath); err != nil {
		os.Stderr.WriteString("E3: Output folder " + output_filepath + " doesn't exist.\n")
		os.Exit(3)
	}

	// Choose a (language specific) tagset. Only Czech and Slovak are supported.
	// (The differences between the Czech and Slovak tags are tiny.)
	if user_specified_language == "ces" {
		tagset = createTagset("ces")
	} else if user_specified_language == "slk" {
		tagset = createTagset("slk")
	} else {
		os.Stderr.WriteString("E4: An unknown language was specified.\n")
		os.Exit(4)
	}

	// Main input loop ------------------------------------------------------------------------
	file, err := os.Open(input_filepath)
	if err != nil {
		os.Stderr.WriteString("E5: Couldn't open input file.\n")
		os.Exit(5)
	}
	input_reader := bufio.NewReader(file)
	input_scanner := bufio.NewScanner(input_reader)

	var triplets []Triplet
	var previous_lemma string

	for input_scanner.Scan() {
		// Load a line from the file.
		line := input_scanner.Text()
		// Divide the line into a triplet of lemma–tag–token and normalize the strings.
		triplet := normalizeStrings(strings.Fields(line))
		// Some lemmas have a suffix containing additional information about the paradigm.
		// The following line removes it.
		triplet[0], _, _ = strings.Cut(triplet[0], "_")

		// If the lemma in the current triplet is the same as the lemma in the previous
		// triplet, the current triplet belongs to the same list as the previous one.
		if triplet[0] == previous_lemma {
			triplets = append(triplets, triplet)
		} else {
			// ↑ If the lemmas are not the same, the current list of triplets is processed
			// into a paradigm, and a new list is created.

			// A list is processed into a paradigm only if it's a group of verbs.
			if areTripletsVerbs(triplets) {
				// Process the triplets in a separate goroutine. Concurency should make
				// the program run faster.
				go processTriplets(triplets, tagset, output_filepath)
			}
			triplets = nil                       // get rid of the old list
			triplets = append(triplets, triplet) // fill it with new stuff
		}
		previous_lemma = triplet[0]
	}
	// Because of looping and conditional shenanigans, the last list of triplets needs
	// to be processed separately.
	if areTripletsVerbs(triplets) {
		go processTriplets(triplets, tagset, output_filepath)
	}
}
